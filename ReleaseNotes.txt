YAT history file

1.16.3 (2019/12/14)
===================

Additions
    Class Version
        New method add_dependency_module
        -> see <yat/utils/Version.h>

    Class XString is now marked as deprecated
    use yat::String methods instead

    Class SysUtils
        - new method exec_script: securely exec a shell script with timeout
        -> see <yat/system/SysUtils.h>

Fixes
    Class String
        Infinite loop then calling String::from_num
        -> see <yat/utils/String.h>

    Added missing reference to md5/md5.cpp in src/CMakeLists.txt

1.15.1 (2019/10/01)
===================

Fixes
    Class String
        Infinite loop when calling methods:
            - find_first_of(const std::string& str, size_type pos = 0)
            - iterator insert(iterator pos, char c)
        Added some missing YAT_DECL for Visual C++
        -> see <yat/utils/String.h>

Additions
    typedef yat::md5::md5_t yat::MD5
        -> see <yat/utils/md5.h>

1.15.0 (2019/07/15)
===================

Additions
    class md5::md5_t
        md5sum processor

    New method FileName::copy_with_md5(const std::string& strDest, yat::String* md5sum_str_p, bool keep_metadata=false);
    -> see <yat/file.FileName.h>

1.14.6 (2019/04/16)
===================

Additions
    CPPunit tests (using libcppunit 1.13.2). To be completed...

Changes

    StringTemplate
        Now support substitutions directives 'uc' & 'lc' (which means upper-case & lower-case respectively):
        If MY_VAR is "Foo" then $(uc:MY_VAR) will be replaced by "FOO" and $(lc:MY_VAR) will replaced by "foo"
        -> see <yat/utils/String.h>

Fixes
    Bug fixed in StringUtil::extract_token(std::string* str_p, char cLeft, char cRight, std::string *pstrToken)
    -> see <yat/utils/String.h>
    In FileName::FSBytes changed the base unit: 1000 instead of 1024
    -> see <yat/file.FileName.h>

1.14.5 (2019/02/13)
===================

Fixes
    Fixed incorrect error management in yat::URI::parse
    Added '|' character in allowed list of characters in the query part

1.14.4 (2018/11/06)
===================

Fixes
    Bad calls of std::string methods from yat::String

1.14.3 (2018/10/10)
===================

Fixes
    Added missing implementation of method yat::String::str_format()
    Added missing method:
        yat::String::split(char c, std::vector<yat::String> *vec_p, bool clear_vector=true);
        yat::String::split(char c, std::vector<yat::String> *vec_p, bool clear_vector=true) const;
        yat::String::operator+=(char c);
        yat::StringUtil::split(std::string* str_p, char c, std::vector<yat::String> *vec_p, bool clear_vector=true);
        yat::StringUtil::split(const std::string& str, char c, std::vector<yat::String> *vec_p, bool clear_vector=true) const;
        yat::File::load(yat::string *);
        yat::SysUtils::get_env(const std::string &strVar, yat::String *pstrValue, const char *pszDef=NULL);

1.14.1 (2018/09/27)
===================

Additions

    New methods in yat::Dictionary
        void to_vector(std::vector<std::string>* vec_p, char sep_key=':');
        void to_string(std::string* str_p, char sep_pair='\n', char sep_key=':');
        -> See <yat/utils/Dictionary.h>

    New methods in yat::Time
        std::string to_local_ISO8601_micro() const;
        std::string to_ISO8601_micro() const;
        std::string to_ISO8601_micro_TU() const;
        -> See <yat/time/Time.h>

Changes

    Old String class no longer deprecated!! It was refactored and is now fully usable
    It does no longer inherit from std::string (which was a big error) but fully compatible with it
    -> See <yat/utils/String.h>

Fixes

    Classes RLockFile & WLockFile
        constructors was not public
        -> See <yat/file/FileName.h>

1.13.17 (2018/07/23)
====================

Additions

    void URI::clear();
    bool URI::empty();
    -> See <yat/Utils/URI.h>

    Macro YAT_FREQUENCY_LIMITED_STATEMENT
    Use this macro to limit the execution frequency of a specific statement, for instance
    a log message in a high frequency loop
    -> See <yat/utils/Logging.h>

Changes

    static ExtractTokenRes StringUtil::extract_token(std::string* str_p, char c, std::string *pstrToken,
                                       bool apply_escape = false);
    static ExtractTokenRes StringUtil::extract_token_right(std::string* str_p, char c, std::string *pstrToken,
                                       bool apply_escape = false);
        'apply_escape' is a new parameter to allow the sperarator character as a part of the tokens
        -> See <yat/utils/String.h>

1.13.16 (2018/04/06)
====================

Additions

    bool StringTemplate::substitute_ex(std::string *pstrTemplate, std::vector<std::string>* not_found_p);
        The new method return 'false' if at leat one substitution fail and returns a list of unresolved symbols
        -> <yat/utils/StringTemplate.h>

Fixes

    UDPListener::Config
        Bug in the constructor
        -> See <yat/network/UDPListener.h>

1.13.15 (2018/03/09)
====================

Additions

    void Time::from_string(const std::string& date_time, const std::string& format);
        Set date/time object from a formatted string
        -> See <yat/time/Time.h>

Changes

    THROW_YAT_EXCEPTION & RETHROW_YAT_EXCEPTION
        The description field now support stream-like notation
        -> See <yat/Exception.h>
Fixes

    YAT_LOG_EXCEPTION
        Thread-safety
        -> See <yat/utils/Logging.h>

1.13.12 (2017/12/22)
====================

Additions

    YAT_VERBOSE, YAT_INFO, YAT_WARNING, YAT_ERROR
        New logging macro to be use as streams
        -> See <yat/utils/Logging.h>

    void FileName::set_path(const std::string& strPath);
        New method to set the path part
        -> See <yat/file/FileName.h>

Fixes

    FileName::name_ext()
        An unexpected exception was throwed in the case of the full path was an empty string


1.13.9 (2017/07/11)
===================

Additions

    class picojison
        Added open source picojson implementation
        -> See <yat/utils/picojson.h>

1.13.6 (2017/03/31)
===================

Additions

    FileName::FileName(const std::string& strPath, const std::string& strName, const std::string& strExt);
        New constructor
        -> See <yat/file/FileName.h>

    bool SysUtils::is_root();
        Return 'True' if the current process is executed as root
        -> See <yat/system/sysUtils.h>

    static void URI::pct_encode(std::string* to_encode, const std::string& reserved=URI_RESERVED);
    static void URI::pct_decode(std::string* to_encode);
        Percent encoding/decoding to allow using reserved characters in URIs. Automatically called when needed.
        -> See <yat/utils/URI.h>

    StringDictionary::StringDictionary(const std::vector<std::string>& vec, char sep);
    StringDictionary::StringDictionary(const std::string& s, char sep_pair, char sep_key);
        New constructors
        -> See <yat/utils/Dictionary.h>

    typedef std::vector<std::string> type StringVector;
        -> See <yat/utils/String.h>

1.13.5 (2017/02/08)
===================

Additions

    UDPTrigger::next_trigger
        Allow manually sending udp frames. Parameter {{trigger_period_ms}} must be set to '0'.
        -> See <yat/network/UDPTrigger.h>

1.13.4 (2016/12/05)
===================

Additions

    Class FileName::FSBytes
    Class FileName::FSStat
    FileName::FSStat FileName::file_system_statistics() const
        Return statistics about a file system
        -> See <yat/file/FileName.h>

1.13.1 (2016/07/20)
===================

Additions

    class LockFile
        SWMR (Single Writer Multiple Reader) file locking class
        -> See <yat/file/FileName.h>

    class AutoLockFile
        Convenience class to ensure unlocking
        -> See <yat/file/FileName.h>

    class Dictionary
        Simple dictionary class using an underlying std::map object
        -> See <yat/utils/Dictionary.h>

    class UDPListener
        FORWARDS THE UDP PACKETS (SENT BY THE SPI BOARD) TO A SPITASK
        -> See <yat/network/UDPListener.h>

1.10.5 (2016/02/11)
===================

 Additions

    class FileName::Info
        A structure used to describe a file
        -> See <yat/file/FileName.h>
    void FileName::info( FileName::Info* ) const
        Get information about a file
        -> See <yat/file/FileName.h>

    template<typename T> bool fp_is_equal(T, a, T b, T precision)
        Floating point comparison operator
        -> See <yat/CommonHeader.h>

    template<typename T> class UniquePtr
        Unique pointer implementation
        -> See <yat/memory/UniquePtr.h>


    bool Pulser::is_done()
        Check pulser activity
        -> See <yat/threading/Pulser.h>




